terraform {
  backend "s3" {
    profile = "optima"
    region  = "eu-west-2"
    bucket  = "tf-account-state"
    key     = "optima-account-key"
  }
}
